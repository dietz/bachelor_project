import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib import rcParams

def compute_total_size():
    print(df_malloc['malloc_size'].agg(sum))

def malloc_free():
    count = df.groupby('function').size().to_latex(folder + "usrbin.tex")
    count = dg.groupby('function').size().to_latex(folder + "documents.tex")
    count = dh.groupby('function').size().to_latex(folder + "epfl.tex")


def plot_timeline():
    df_malloc['index'] = df_malloc.index
    timeline = sns.catplot(x='index', y='stacktrace', hue='function',
                           data=df_malloc, aspect=4/2)

    timeline.savefig(folder + "timeline.png")

def size_distribution():
    # Number of requests by groupe of sizes
    sns.distplot(df_malloc['malloc_size'], kde=False, axlabel="requested sizes").get_figure().savefig(folder + str(1) + "_sizes_distribution.png")
    sns.distplot(dg_malloc['malloc_size'], kde=False, axlabel="requested sizes").get_figure().savefig(folder + str(2) + "_sizes_distribution.png")
    sns.distplot(dh_malloc['malloc_size'], kde=False, axlabel="requested sizes").get_figure().savefig(folder + str(0) + "_sizes_distribution.png")


def callers_of_malloc():
    # Distribution of sizes per caller in malloc
    ##sizes_per_caller = sns.jointplot(x='stacktrace', y='malloc_size',data=df_malloc, height=10)
    sizes_per_caller0 = sns.jointplot(x="stacktrace", y="malloc_size", data=df_malloc, height=7)
    sizes_per_caller1 = sns.jointplot(x="stacktrace", y="malloc_size", data=dg_malloc, height=7)
    sizes_per_caller2 = sns.jointplot(x="stacktrace", y="malloc_size", data=dh_malloc, height=7)

    sizes_per_caller0.fig.get_axes()[0].set_yscale('log')
    sizes_per_caller1.fig.get_axes()[0].set_yscale('log')
    sizes_per_caller2.fig.get_axes()[0].set_yscale('log')

    sizes_per_caller0.savefig(folder + str(0) + "_sizes_per_caller.png")
    sizes_per_caller1.savefig(folder + str(1) + "_sizes_per_caller.png")
    sizes_per_caller2.savefig(folder + str(2) + "_sizes_per_caller.png")

   # stack_group = df_malloc.groupby('stacktrace').size()
   # sns.jointplot(x=0, y=1, data=stack_group).savefig(folder + "activity_of_each_caller.png")

sns.set_context("paper")
sns.set(style="darkgrid")
#  headers
headers = ["stacktrace", "malloc_size", "result/tofree", "function"]
folder = "parsing_and_plots/"

# Initial dataframe
df = pd.read_csv("data/extract_" + str(0) + ".csv", names=headers)
dg = pd.read_csv("data/extract_" + str(1) + ".csv", names=headers)
dh = pd.read_csv("data/extract_" + str(2) + ".csv", names=headers)
# Parsed data
df_malloc = df[df.function == 'malloc']
dg_malloc = dg[dg.function == 'malloc']
dh_malloc = dh[dh.function == 'malloc']

malloc_free()
size_distribution()
callers_of_malloc()
plot_timeline()
compute_total_size()
