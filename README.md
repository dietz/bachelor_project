# Project handout
* Get familiar with memory allocators and secure memory allocators by reading
DieHard and DieHard(er) papers as well as some follow up related work (Ahmad
will help you find some pointers)
* Develop an interception mechanism that allows you to record all memory
allocations/deallocations by intercepting malloc/free and recording a)
allocation site, b) allocation size, c) free site, d) use of indirection for
allocation (e.g., going through a custom allocator)
* Study different applications and their allocation patterns
* Evaluate applications like SPEC CPU2006/2017, server applications, and desktop
applications
* Report your findings in paper format that describes the setup and your analysis
* Present the results in front of the group
