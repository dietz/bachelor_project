\documentclass{article}

\usepackage{graphicx}
\usepackage{hyperref}

\title{Bachelor Project}
\date{10-01-2020}
\author{Samuel Dietz}

\begin{document}
\maketitle

%----------------------------------------------------------------------------%
\section{Overview of the Project}
The project was under the direction of Professor Mathias Payer and Doctoral Assistant Ahmad Hazimeh.\\
Below are the goals of the project as stated by Professor Payer :
\begin{itemize}
  \item Get familiar with memory allocators and secure memory allocators by reading
    DieHard and DieHard(er) papers as well as some follow up related work (Ahmad
    will help you find some pointers)
  \item Develop an interception mechanism that allows you to record all memory
    allocations/deallocations by intercepting malloc/free and recording a)
    allocation site, b) allocation size, c) free site, d) use of indirection for
    allocation (e.g., going through a custom allocator)
  \item Study different applications and their allocation patterns
  \item Evaluate applications like SPEC CPU2006/2017, server applications, and desktop
    applications
  \item Report your findings in paper format that describes the setup and your analysis
  \item Present the results in front of the group
\end{itemize}
\\
The document is structured as follows : we first present an interception
mechanism as well as the tools it uses then briefly mention the
frameworks meant to parse and analyse the data.\\
We then make use of the setup to extract some observations out of an application.\\
Lastly I take a short time speaking of the project and my overall experience.\\

%----------------------------------------------------------------------------%
\section{Setup}
The whole project is available on the \href{https://www.wikibooks.org}{EPFL's GitLAB
  repository}.
%----------------------------------------------------------------------------%
%----------------------------------------------------------------------------%
\subsection{Extracting}
%----------------------------------------------------------------------------%
%----------------------------------------------------------------------------%
%----------------------------------------------------------------------------%
\subsubsection{Structure}
The core mechanism of the system is the shared object \textit{csv\_writer.so} (and its
less useful brother \textit{stdout\_writer.so}) located in
the project's writer directory.\\
It is compiled with the help of a Makefile and the flags -shared and -fPIC of
GCC. Its source code is the \textit{my\_malloc.c} file and its dependency \textit{writer.h}\\
\textit{writer.h} declares two functions implemented by either \textit{stdout\_writer.c} or
\textit{csv\_writer.c}.\\

To ease the use of the shared object a bash script called \textit{extract.sh} is available at the
project's root but was not meant to be generic (it was at first but ended up being a luxurythat we could not affort during the late stages of the project) and must be modified
accordingly to the user's needs.

%----------------------------------------------------------------------------%
%----------------------------------------------------------------------------%
%----------------------------------------------------------------------------%
\subsubsection{Tools used by the setup}

The setup is an interception mechanism of the allocations and deallocations of
an already compiled program (hence dynamically linked).\\ Thus it must detect
every call to \textit{malloc()} and \textit{free()}. It is done by linking the shared object above
with the use of the variable LD\_PRELOAD from the GNU linker LD.\\

The shared object uses "hooks" provided by GLIBC to perform its task :\\
When the application calls malloc or free, the call is redirected to the hooks
(which are given the caller's stacktrace !). These hooks must be implemented
by the apprentice hacker and in the project these functions use
\textit{writer.h}
to extract info on a either a csv file or the Standard Output.\\

These hooks are provided by GLIBC's allocator and work the following way, from \textit{man malloc\_hook} :\\
\textbf{The variable \_\_malloc\_initialize\_hook points at a function that is called once when the malloc implementation is initialized. This is a weak variable, so it can be overridden  in the application...}
%----------------------------------------------------------------------------%
%----------------------------------------------------------------------------%
\subsection{Parsing and plotting}
We make use of the Pandas and Seaborn Python frameworks to build visual
representations of the data. The code is located in
\textit{parser/csv\_parser.py}.
%----------------------------------------------------------------------------%
\section{Some Analysis}
%----------------------------------------------------------------------------%
%----------------------------------------------------------------------------%
\subsection{The \textit{tree} application}
\textbf{Tree is a recursive directory listing program that produces a depth
  indented listing of files...} - from \textit{man tree}.\\
We chose this application because of the ease we had to obtain very different
results (in size of the data extracted) with different contexts.\\
Three runs of Tree were made with the following inputs :\\
\begin{enumerate}
  \item /usr/bin (4 directories, 4407 files)
  \item /home/\textit{my\_usernamme}/Documents (14 directories, 257 files)
  \item /home/\textit{my\_usernamme}/\textit{my\_epfl\_files} (2813
    directories, 26506 files)
\end{enumerate}
We decided for the study to focus on the statistics of malloc alone (the
distribution of requested sizes) and of malloc in relation with the heap, that
is, which part of the memory requests which sizes ?\\
%----------------------------------------------------------------------------%
%----------------------------------------------------------------------------%
\subsection{Issue with the tool}
But before any analysis, we met an unexpected result while counting the number of calls
to \textit{malloc()} and \textit{free()}:\\
\begin{table}[!htbp]
  \centering
\begin{tabular}{l|l|l|l|}
\cline{2-4}
                                        & \multicolumn{3}{l|}{Input Number} \\ \cline{2-4}
                                        & 1         & 2         & 3         \\ \hline
\multicolumn{1}{|l|}{Calls to free()}   & 14743      & 99277     & 1061     \\ \hline
\multicolumn{1}{|l|}{Calls to malloc()} & 32        & 32        & 32        \\ \hline
\end{tabular}
\end{table}

Independent from the input the amount of calls to malloc remained the same
whereas the number of calls to free increased more or less linearly with the size of the
input.\\
From our knowledge of the allocations and deallocations these result do not
make sense because these operations should work by pairs thus be of the same
amount, and it is counter-intuitive that the number of allocation does not
grow.\\
To prove the results are wrong we use \textit{Heaptrack}, a heap profiler:\\
\begin{table}[!htbp]
  \centering
\begin{tabular}{l|l|l|l|}
\cline{2-4}
                                        & \multicolumn{3}{l|}{Input Number} \\ \cline{2-4}
                                        & 1         & 2         & 3         \\ \hline
                                      \multicolumn{1}{|l|}{Calls to allocation functions} & 17,652        & 102,940        & 3,756        \\ \hline
\end{tabular}
\end{table}

It is not told what are these "allocation functions" but apart from huge requests which are handled by functions such as \textit{mmap} all the derivatives of \textit{malloc()} are just wrapper of this function, hence should be recorded by the tool.\\\\
One explanation would be reason why the "hooks" are deprecated : they are not thread safe and this can be easily be shown by taking a look at \textit{my\_malloc.c}:\\
During each run of one of the hook functions, global variables are modified without any mechanism to ensure consistency.\\
Due to time constraints we were not able to point out the reasons for what made the tool unusable which was discovered late into the project.

%----------------------------------------------------------------------------%
%----------------------------------------------------------------------------%
\subsection{Some insight of \textit{Tree}'s behavior}
Heaptrack provides a neat histogram which shows the number of allocations per range of required sizes : see \ref{fig:usr}, \ref{fig:doc} and \ref{fig:epfl}.\\
This illustrates the correlation between the size of the input and the number of allocations, this is because \textit{Tree} must read every inode it encouters on its path this is expected.\\
As the input grows, we observe that a specific range of sizes is requested by the software and ends up dominating all the other ranges with the \textit{my\_epfl\_files}.

\begin{figure}[h!]
  \centering
  \begin{subfigure}
    \includegraphics[width=0.5\linewidth]{../data_and_plots/heaptrack_usrbin.png}
    \caption{"tree /usr/bin"}
    \label{fig:usr}
  \end{subfigure}
  \begin{subfigure}
    \includegraphics[width=0.5\linewidth]{../data_and_plots/heaptrack_documents.png}
    \caption{"tree /home/\textit{my\_usernamme}/Documents"}
    \label{fig:doc}
  \end{subfigure}
  \begin{subfigure}
    \includegraphics[width=0.5\linewidth]{../data_and_plots/heaptrack_epfl.png}
    \caption{"tree /home/\textit{my\_usernamme}/\textit{my\_epfl\_files}"}
    \label{fig:epfl}
  \end{subfigure}
\end{figure}

Unfortunately heaptrack does not provide a way to visualize the locations of the calls in the stack.

%----------------------------------------------------------------------------%
\section{Conclusion}
%----------------------------------------------------------------------------%
%----------------------------------------------------------------------------%
%----------------------------------------------------------------------------%
\subsubsection{Thoughts on the tool}
Contrary to \href{https://github.com/KDE/heaptrack}{Heaptrack} and its brother \href{http://valgrind.org/docs/manual/ms-manual.html#ms-manual.theoutputpreamble}{Valgrind's Massif} this tool provides raw data that can be more easily manipulated with the help of frameworks like Pandas. However it is way to primal in terms of extraction and analysis capabilities : it cannot extract everything and for example cannot report which function of the application required most of the allocations.\\
Using the source code of \textit{Heaptrack} it could be enhanced to reach satisfying results. The combined use of this version and efficient parsing and ploting frameworks linked alltogether with a simple bash script would form an efficient tool for basic analysis. Hovewer for more advanced features, the applications above should be options to consider.

%----------------------------------------------------------------------------%
%----------------------------------------------------------------------------%
%----------------------------------------------------------------------------%
\subsubsection{Personnal Thoughts}
I greatly underestimated the obstacles I would encounter during the course of the project, from having endless wrestling fights with the \textit{Python} frameworks to obtain decent plots to failing to understand that the study of the result took a greater part of the project than building the hooking tool (even so this part was fun), and also failing to correctly communicate with the teaching team to be more efficent (I take note : do note refuse the teacher's offer to use Slack - if he makes this offer there's a good chance it will be beneficial).\\
But this does not erase the fact that most of what is reported above and written in the repository was produced after the end of semester.\\
I also learned about allocators, C, discovered Python and got an insight of what is research.

\end{document}
