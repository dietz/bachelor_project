  static void *
my_malloc_hook(size_t size,
               const void *caller) {
  void *result;

  __malloc_hook = old_malloc_hook;

  result = malloc(size);

  old_malloc_hook = __malloc_hook;

  malloc_write(size, (intptr_t) caller,
                     (intptr_t) result);

  __malloc_hook = my_malloc_hook;
  return result;
}
