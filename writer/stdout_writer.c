#include "writer.h"

int malloc_write(size_t size, intptr_t stackTrace, intptr_t result) {

  printf("malloc(%u) called from %lu returns %lu \n",
      (unsigned int) size, stackTrace, result);

  return 0;
}

int free_write(intptr_t stackTrace, intptr_t to_free) {

  printf("free(%lu) called from %lu\n", to_free, stackTrace);

  return 0;
}
