#include "writer.h"

int malloc_write(size_t size, intptr_t stackTrace, intptr_t result) {

  FILE* file  = fopen("./data/extract.csv", "a");

  if (file == NULL) {
    fprintf(stderr, "Error : could not create/open file\n");
    return 1;
  }

  fprintf(file, "%lu,%u,%lu,malloc\n", stackTrace, (unsigned int) size, result);

  fclose(file);

  return 0;
}

int free_write(intptr_t stackTrace, intptr_t to_free) {

  FILE* file  = fopen("./data/extract.csv", "a");

  if(file == NULL) {
    fprintf(stderr, "Error : could not create/open file\n");
    return 1;
  }

  fprintf(file, "%lu,,%lu,free\n", stackTrace, to_free);

  fclose(file);

  return 0;
}
