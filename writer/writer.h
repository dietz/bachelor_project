#include <stdio.h>
#include <stdint.h>
#include <string.h>

/* Write @elements to the writer ; returns 0 or 1 in case of failure */
int malloc_write(size_t size, intptr_t stackTrace, intptr_t result);

/* Write @elements to the writer ; returns 0 or 1 in case of failure */
int free_write(intptr_t stackTrace, intptr_t to_free);
