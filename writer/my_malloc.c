#include <stdint.h>
#include <stdio.h>
#include <malloc.h>
#include "writer.h"

/* Prototypes for our hooks.  */
static void my_init_hook(void);
static void *my_malloc_hook(size_t, const void *);
static void my_free_hook(void*, const void*);

/* Variables to save original hooks. */
static void *(*old_malloc_hook)(size_t, const void *);
static void (*old_free_hook)(void*, const void *);

/* Override initializing hook from the C library. */
void (*__malloc_initialize_hook) (void) = my_init_hook;

static void my_init_hook(void)
{
  old_malloc_hook = __malloc_hook;
  __malloc_hook = my_malloc_hook;

  old_free_hook = __free_hook;
  __free_hook = my_free_hook;
}

  static void *
my_malloc_hook(size_t size, const void *caller)
{
  void *result;

  /* Restore all old hooks */
  __malloc_hook = old_malloc_hook;

  /* Call recursively */
  result = malloc(size);

  /* Save underlying hooks */
  old_malloc_hook = __malloc_hook;

  /* Pass data to writer */
  malloc_write(size, (intptr_t) caller, (intptr_t) result);

  /* Restore our own hooks */
  __malloc_hook = my_malloc_hook;

  return result;
}

  static void
my_free_hook(void *ptr, const void *caller)
{
  __free_hook = old_free_hook;

  free(ptr);

  old_free_hook = __free_hook;

  free_write((intptr_t) caller, (intptr_t) ptr);

  __free_hook = my_free_hook;
}
