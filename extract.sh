#!/bin/bash

extract() {
  echo ${arr[${i}]}
  app=${arr[${i}]}

  echo Extracting info from $app...
  LD_PRELOAD=writer/csv_malloc.so $app
  echo End of the application execution...

  echo Renaming extract.csv in extract_${i}.csv...
  mv data/extract.csv data/extract_${i}.csv
  echo
}

arr[0]='tree /usr/bin'
arr[1]='tree /home/andrea/Documents'
arr[2]='tree /home/andrea/_EPFL'

#arr[3]='cp contexts/100MB_size.txt >dump'
#arr[4]='cp contexts/500MB_size.txt >dump'
#arr[5]='cp contexts/1000MB_size.txt >dump'

#arr[3]='cat contexts/100MB_size.txt >dump'
#arr[4]='cat contexts/500MB_size.txt >dump'
#arr[5]='cat contexts/1000MB_size.txt >dump'

for (( i=0; i<=2; i++ ))
do
  extract
done

echo Parsing and plotting must be done through the parser/csv_parser.py python file.
